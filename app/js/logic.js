$(document).ready(function() {

    urlConstant= "http://192.168.75.84:7810/geohash";
    var searchMarkers = [];
    var all_overlays = [];
    var coordinates_array = [];
    var polygonArray = [];
    var str_coordinates = '';
    var bounds = new google.maps.LatLngBounds();   //defining bounds
    var infoWindow = new google.maps.InfoWindow();   //defining infowindow
    var drawingManager;
    const MapLatLong= {
        "lat": 31.5140232,
        "lng": 74.341254
    };
    var show_add_area_btn = 0;
    var show_add_areaname_btn = 0;

    initMap =  function() {
        var map = {
            zoom: 8,
            center: new google.maps.LatLng(MapLatLong.lat, MapLatLong.lng),
            pan: true
        };
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
         mapContainer = new google.maps.Map(document.getElementById('map'), map);
        // Create the search box and link it to the UI element.

        // Bias the SearchBox results towards current map's viewport.
        mapContainer.addListener('bounds_changed', function () {
            searchBox.setBounds(mapContainer.getBounds());
        });


            document.getElementById('loading').style.display="block";

            //showing added regions on map
            var url = urlConstant + '/get_all_regions';
            $.post(url, {
            }, function (data) {
                document.getElementById('loading').style.display="none";
                var dataArray = [];
                var excelArray = [];
                if (typeof(data) == "string")
                    data = JSON.parse(data);

                if (data.status === 200) {
                    var boundsArray = [];
                    var areaList = data.regions;
                    areaList.forEach(function (column) {
                        polygonArray = [];
                        var polygon = column.region_path_string.split(',');
                        for (var j = 0; j < (polygon.length - 1); j++) {
                            polygon[j] = polygon[j].split(" ");
                            polygonArray.push({
                                lat: parseFloat(polygon[j][0]), lng: parseFloat(polygon[j][1])
                            })
                            var areaBounds = new google.maps.LatLng(parseFloat(polygon[j][0]), parseFloat(polygon[j][1]));
                            bounds.extend(areaBounds);
                        }
                        var shape = new google.maps.Polygon({
                            paths: polygonArray,
                            map: mapContainer,
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 3,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35
                        });
                        shape.content = '<div class="infoWindowContent">' +
                            '<center><b>Region Name</b></center>' +
                            '<span> ' + column.region_name + '</span><br>'
                        '</div>';
                        google.maps.event.addListener(shape, 'click', function (event) {
                            infoWindow.setContent(shape.content);
                            infoWindow.setPosition(event.latLng);
                            infoWindow.open(mapContainer,shape);
                        });
                        mapContainer.fitBounds(bounds);
                        shape.setMap(mapContainer);

                    });

                }
            });


//============================================================================================================================
//=================Listen for the event fired when the user selects a prediction and retrieve from search box=================
//============================================================================================================================

        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            searchMarkers.forEach(function (marker) {
                marker.setMap(null);
            });
            searchMarkers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                searchMarkers.push(new google.maps.Marker({
                    map: mapContainer,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            mapContainer.fitBounds(bounds);
        });
        // [END region_getplaces]
        var polygOption = new google.maps.Polygon({

            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            editable: true
        });
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.MARKER,
            drawingControl: true,

            drawingControlOptions: {

                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    // google.maps.drawing.OverlayType.MARKER,
                    // google.maps.drawing.OverlayType.CIRCLE,
                    google.maps.drawing.OverlayType.POLYGON,
                    // google.maps.drawing.OverlayType.POLYLINE,
                    // google.maps.drawing.OverlayType.RECTANGLE
                ]
            },
            polygonOptions: {
                fillColor: '#0099FF',
                fillOpacity: 0.7,
                strokeColor: '#AA2143',
                strokeWeight: 2,
                editable: true
            }
            // markerOptions: {icon: 'app/img/freeDriver.png'},
            // circleOptions: {
            //     fillColor: '#909fa7',
            //     fillOpacity: 1,
            //     strokeWeight: 1,
            //     clickable: false,
            //     editable: true,
            //     zIndex: 1
            // }
        });


        //getting latlongs on overlay complete
        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
            all_overlays.push(event);
            coordinates_array = [];
            drawingManager.setMap(null);
            if (event.type == google.maps.drawing.OverlayType.POLYGON) {
                // Switch back to non-drawing mode after drawing a shape.
                drawingManager.setDrawingMode(null);    //hiidng the drawing tool aftre completion of polygon
                var length = event.overlay.getPath().length;
                event.overlay.getPath().forEach(function(data){
                    var latlong = data.lat() + " " + data.lng();
                    coordinates_array.push(latlong)
                });
                latLongArrayList(coordinates_array);
                // if(google.maps.geometry.poly.containsLocation(event.latLng, polygonArray) == true) {
                // }

            }

            // if (event.type != google.maps.drawing.OverlayType.POLYGON) {

            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            //     var newShape = event.overlay;
            //     newShape.type = event.type;
            // google.maps.event.addListener(newShape, 'click', function(event) {
            //     var mEvent = event;
            //     alert("click listner");
            //     all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //     $scope.latLongArrayList(coordinates_array);
            //     google.maps.event.addListener(newShape.getPath(), 'set_at', function(mEvent) {
            //         //console.log("set_at");
            //         console.log("set_at\n",JSON.stringify(coordinates_array));
            //         // all_overlays.push(event);
            //         // var length = event.overlay.getPath().length;
            //         // event.overlay.getPath().forEach(function(data){
            //         //     var latlong = data.lat() + " " + data.lng();
            //         //     coordinates_array.push(latlong);
            //         // });
            //         // $scope.latLongArrayList(coordinates_array);
            //     });

            //     google.maps.event.addListener(newShape.getPath(), 'insert_at', function(mEvent) {
            //         //console.log("insert_at");
            //         console.log("insert_at\n",JSON.stringify(coordinates_array));
            //         // all_overlays.push(event);
            //         // var length = event.overlay.getPath().length;
            //         // event.overlay.getPath().forEach(function(data){
            //         //     var latlong = data.lat() + " " + data.lng();
            //         //     coordinates_array.push(latlong);
            //         // });
            //         // $scope.latLongArrayList(coordinates_array);
            //     });
            //     google.maps.event.addListener(newShape.getPath(), 'dragstart', function(mEvent) {
            //         //console.log("insert_at");
            //         console.log("dragstart\n",JSON.stringify(coordinates_array));
            //         // all_overlays.push(event);
            //         // var length = event.overlay.getPath().length;
            //         // event.overlay.getPath().forEach(function(data){
            //         //     var latlong = data.lat() + " " + data.lng();
            //         //     coordinates_array.push(latlong);
            //         // });
            //         // $scope.latLongArrayList(coordinates_array);
            //     });
            //     google.maps.event.addListener(newShape.getPath(), 'dragend', function(mEvent) {
            //         //console.log("insert_at");
            //         console.log("dragend\n",JSON.stringify(coordinates_array));
            //         // all_overlays.push(event);
            //         // var length = event.overlay.getPath().length;
            //         // event.overlay.getPath().forEach(function(data){
            //         //     var latlong = data.lat() + " " + data.lng();
            //         //     coordinates_array.push(latlong);
            //         // });
            //         // $scope.latLongArrayList(coordinates_array);
            //     });
            //     google.maps.event.addListener(newShape.getPath(), 'coordinates_changed', function(mEvent) {
            //         //console.log("insert_at");
            //         console.log("coordinates_changed\n",JSON.stringify(coordinates_array));
            //         // all_overlays.push(event);
            //         // var length = event.overlay.getPath().length;
            //         // event.overlay.getPath().forEach(function(data){
            //         //     var latlong = data.lat() + " " + data.lng();
            //         //     coordinates_array.push(latlong);
            //         // });
            //         // $scope.latLongArrayList(coordinates_array);
            //     });
            //  // setSelection(newShape);
            // });
            // google.maps.event.addListener(newShape, 'dragstart', function(event) {

            //     alert("dragstart listner");
            //     google.maps.event.addListener(newShape.getPath(), 'set_at', function() {
            //         //console.log("set_at");
            //         console.log("set_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });

            //     google.maps.event.addListener(newShape.getPath(), 'insert_at', function() {
            //         //console.log("insert_at");
            //         console.log("insert_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });
            //  // setSelection(newShape);
            // });
            // google.maps.event.addListener(newShape, 'dragend', function(event) {

            //     alert("dragend listner");
            //     google.maps.event.addListener(newShape.getPath(), 'set_at', function() {
            //         //console.log("set_at");
            //         console.log("set_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });

            //     google.maps.event.addListener(newShape.getPath(), 'insert_at', function() {
            //         //console.log("insert_at");
            //         console.log("insert_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });
            //  // setSelection(newShape);
            // });
            // google.maps.event.addListener(newShape, 'coordinates_changed', function(event) {

            //     alert("coordinates_changed listner");
            //     google.maps.event.addListener(newShape.getPath(), 'set_at', function() {
            //         //console.log("set_at");
            //         console.log("set_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });

            //     google.maps.event.addListener(newShape.getPath(), 'insert_at', function() {
            //         //console.log("insert_at");
            //         console.log("insert_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });
            //  // setSelection(newShape);
            // });
            // google.maps.event.addListener(newShape, 'dragend', function(event) {

            //     alert("click listner");
            //     google.maps.event.addListener(newShape.getPath(), 'set_at', function() {
            //         //console.log("set_at");
            //         console.log("set_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });

            //     google.maps.event.addListener(newShape.getPath(), 'insert_at', function() {
            //         //console.log("insert_at");
            //         console.log("insert_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });
            //  // setSelection(newShape);
            // });
            // google.maps.event.addListener(newShape, 'dragend', function(event) {

            //     alert("click listner");
            //     google.maps.event.addListener(newShape.getPath(), 'set_at', function() {
            //         //console.log("set_at");
            //         console.log("set_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });

            //     google.maps.event.addListener(newShape.getPath(), 'insert_at', function() {
            //         //console.log("insert_at");
            //         console.log("insert_at\n",JSON.stringify(coordinates_array));
            //         all_overlays.push(event);
            //         var length = event.overlay.getPath().length;
            //         event.overlay.getPath().forEach(function(data){
            //             var latlong = data.lat() + " " + data.lng();
            //             coordinates_array.push(latlong);
            //         });
            //         $scope.latLongArrayList(coordinates_array);
            //     });
            //  // setSelection(newShape);
            // });
            // }
        });
        drawingManager.setMap(mapContainer);
        drawingManager.setDrawingMode(null);   //on load disabling the default tool for drawing

        drawingManager.enableCoordinatesChangedEvent = function(){
            var me = this,
                superClass = me.superClass,
                isBeingDragged = false,
                triggerCoordinatesChanged = function(){
                    //broadcast normalized event
                    google.maps.event.trigger(superClass,'coordinates_changed');
                };

            //if  the overlay is being dragged, set_at gets called repeatedly, so either we can debounce that or igore while dragging, ignoring is more efficient
            google.maps.event.addListener(superClass,'dragstart',function(){
                isBeingDragged = true;
            });

            //if the overlay is dragged
            google.maps.event.addListener(superClass,'dragend',function(){
                triggerCoordinatesChanged();
                isBeingDragged = false;
            });

            //or vertices are added to any of the possible paths, or deleted
            var paths = superClass.getPaths();
            paths.forEach(function(path,i){
                google.maps.event.addListener(path,"insert_at",function(){
                    triggerCoordinatesChanged();
                });
                google.maps.event.addListener(path,"set_at",function(){
                    if(!isBeingDragged){
                        triggerCoordinatesChanged();
                    }
                });
                google.maps.event.addListener(path,"remove_at",function(){
                    triggerCoordinatesChanged();
                });
            });

        };
    }
    //REMOVING SHAPE ON CLICK
    removeShape = function () {

        coordinates_array = [];
        var areaName = "";
        str_coordinates = '';
        for (var i = 0; i < all_overlays.length; i++) {
            all_overlays[i].overlay.setMap(null);   //removing d alraedy drwan polygon
        }
        all_overlays = [];
        show_add_area_btn = 0;
        show_add_areaname_btn = 0;
        drawingManager.setMap(mapContainer)
        drawingManager.setOptions({
            drawingControl: true
        });

    }
//=========================================================================================================================
//                                  FUNCTION TO PUT LATLONG INTO ARRAY AND SEND TO BACKEND
// =========================================================================================================================
    latLongArrayList = function (coordinates_array) {
        str_coordinates = '';
        var arr_len = coordinates_array.length;
        //console.log(coordinates_array);
        //coordinates_array = coordinates_array.toString();
        //console.log(coordinates_array);

        var new_arr = [];
        for (var i = 0; i < arr_len; i++) {
            var val = coordinates_array[i].split(',');
            var obj = {
                "lat": val[0],
                "lng": val[1]
            }
            str_coordinates += coordinates_array[i].toString() + ","
            new_arr.push(obj);
        }
        str_coordinates += coordinates_array[0].toString() + ","
        str_coordinates = str_coordinates.slice(0, -1);
        //console.log(str_coordinates);   //in string form
        //console.log(new_arr);    //in array form
        show_add_area_btn = 1;
    }

//=========================================================================================================================
//                                  FUNCTION TO ADD AREA
// ========================================================================================================================
    addArea = function ()
    {
        show_add_areaname_btn = 1;
        var areaName=document.getElementById('areaName').value;
        if(areaName==='')
        {
            alert("Invalid Input");
            return false;
        }
        else if( str_coordinates==="")
        {
            alert("Please draw a poly line first.");
            return false;
        }
        document.getElementById('loading').style.display="block";
        const url=urlConstant+"/add_region";
        $.post( url, {
            region_name: areaName,
            region_path: str_coordinates
            }
        ).then(
            function (data) {
                document.getElementById('loading').style.display="none";
                if (typeof(data) == "string") {
                    data = JSON.parse(data);
                }
                if (data.status ===200) {
                   alert(data.message);
                    location.reload();
                }
                else {
                    alert(data.error);
                    removeShape();
                }
            });
    };
    initMap();    //INITIALISING THE MAP WITH DRAWING PLACES FEATURE
});
